# Projeto

Objetivos da disciplina:

- Construir uma aplicação SPA, dividida em módulos de front end e back end, com acesso a banco de dados.


O que será feito:

- Software de negócio, contemplando requisitos pré-definidos, contendo arquitetura orientada a serviços, separada em front, back end e persistência de dados em banco de dados.

Entregáveis:

- Código fonte do front
- Código fonte do back
- Script do banco de dados
- Documentação

